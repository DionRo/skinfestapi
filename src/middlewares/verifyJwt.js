const jwt = require("jsonwebtoken");

// middleware to validate token
 
verifyToken = (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) return res.status(401).json({ error: "Access denied" });
  try {
    token = token.replace('Bearer ', '')
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next(); // to continue the flow
  } catch (err) {
    res.status(400).json({ error: "Token is not valid" });
  }
};

const verifyJwt = {
  verifyToken,
};

module.exports = verifyJwt;