const User = require("../models/user");
const jwt = require("jsonwebtoken");


const Joi = require("joi");

const validationSchema = Joi.object({
  _id: Joi.string(),
  name: Joi.string().min(3).max(255).required(),
  email: Joi.string().min(6).max(255).required().email(),
  password: Joi.string(),
  dob: Joi.string(),
  roles: Joi.array(),
  gallery: {
      _id: Joi.string(),
      name: Joi.string().min(3).max(20).required(),
      about:Joi.string().min(20).max(1024).required(),
      __v: Joi.number(),
      posts: Joi.array(),
      totalViewCount: Joi.number(),
      id: Joi.string(),
  },
  accessToken: Joi.string(),
});

validateBody = (req, res, next) => {
  // validate the user
  const { error } = validationSchema.validate(req.body);
  if (error) {
    return res.status(400).json({ error: error.details[0].message });
  }

  next(); 
};

async function checkDuplicateEmail  (req, res, next) {
    // Email
  const user = await User.findOne({email: req.body.email})

  let token =  req.headers.authorization;
  token = token.replace('Bearer ', '')
  const information = jwt.decode(token);

  const loggedInUser = await User.findById(information._id)


  if (user && user.email !== loggedInUser.email) {
    res.status(400).send({ error: "Failed! Email is already in use!" });
    return;
  }

  next();
   
};


const verifyAccount = {
  validateBody,
  checkDuplicateEmail
};

module.exports = verifyAccount; 