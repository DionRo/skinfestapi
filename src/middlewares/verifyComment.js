const Joi = require("joi");


const validationSchema = Joi.object({
  content: Joi.string().min(1).max(2048).required(),
});

validateBody = (req, res, next) => {
  // validate the user
  const { error } = validationSchema.validate(req.body);
  if (error) {
    return res.status(400).json({ error: error.details[0].message });
  }

  next();
};

const verifyPost = {
  validateBody,
};

module.exports = verifyPost;