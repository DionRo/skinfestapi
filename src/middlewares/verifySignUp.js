const User = require("../models/user");

const Joi = require("joi");

const validationSchema = Joi.object({
  name: Joi.string().min(3).max(255).required(),
  email: Joi.string().min(6).max(255).required().email(),
  dob: Joi.date().required(),
  roles: Joi.required(),
  password: Joi.string().min(6).max(1024).required(),
  galleryName: Joi.string().min(3).max(20).required(),
  galleryAbout: Joi.string().min(20).max(1024).required(),
});

validateBody = (req, res, next) => {
  // validate the user
  const { error } = validationSchema.validate(req.body);
  if (error) {
    return res.status(400).json({ error: error.details[0].message });
  }

  next(); 
};

checkDuplicateEmail = (req, res, next) => {
    // Email
    User.findOne({
      email: req.body.email
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ error: "Failed! Email is already in use!" });
        return;
      }

      next();
    });
};


const verifySignUp = {
  validateBody,
  checkDuplicateEmail
};

module.exports = verifySignUp;