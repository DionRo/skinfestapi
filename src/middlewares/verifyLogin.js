const Joi = require("joi");
const bcrypt = require("bcryptjs");


const validationSchema = Joi.object({
  email: Joi.string().min(6).max(255).required().email(),
  password: Joi.string().min(6).max(1024).required(),
});

validateBody = (req, res, next) => {
  // validate the user
  const { error } = validationSchema.validate(req.body);
  if (error) {
    return res.status(400).json({ error: error.details[0].message });
  }

  next();
};

const verifyLogin = {
  validateBody,
};

module.exports = verifyLogin;