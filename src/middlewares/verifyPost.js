const Joi = require("joi");


const validationSchema = Joi.object({
  _id: Joi.string(),
  title: Joi.string().min(6).max(255).required(),
  content: Joi.string().min(6).max(2048).required(),
  image: Joi.string().required(),
  hasAdultAge: Joi.boolean().required(),
  likes: Joi.number().required(),
  viewAmount: Joi.number().required(),
  createdAt: Joi.string(),
  comments: Joi.array()
});

validateBody = (req, res, next) => {
  // validate the user
  const { error } = validationSchema.validate(req.body);
  if (error) {
    return res.status(400).json({ error: error.details[0].message });
  }

  next();
};

const verifyPost = {
  validateBody,
};

module.exports = verifyPost;