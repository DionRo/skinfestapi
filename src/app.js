const express = require('express')

// this catches an exception in a route handler and calls next with it,
// so express' error middleware can deal with it
// saves us a try catch in each route handler
// note: this will be standard in express 5.0, to be released soon
require('express-async-errors')

const app = express()

const cors = require('cors')
const helmet = require('helmet')

const morgan = require('morgan')

// parse json body of incoming request
app.use(express.json())

// enable CORS (cross origin resourse sharing)
// you don't need it for this example, but you will if you host a frontend
// on a different origin (url)
// https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

app.use(cors());

app.all('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next()
  });

// not the topic of this example, but good to be aware of security issues
// helmet sets headers to avoid common security risks
// https://expressjs.com/en/advanced/best-practice-security.html
app.use(helmet())

// use morgan for logging
app.use(morgan('dev'))

const errors = require('./errors')

// routes Authentication
const authRoutes = require("./routes/auth.routes");
app.use('/api/auth', authRoutes)

// routes User
const userRoutes = require("./routes/user.routes");
app.use('/api/user', userRoutes)

// routes Gallery
const galleryRoutes = require("./routes/gallery.routes");
app.use('/api/gallery', galleryRoutes)

// routes Blog
const blogRoutes = require("./routes/blog.routes");
app.use('/api/blog', blogRoutes)

// routes Comment
const commentRoutes = require("./routes/comment.routes");
app.use('/api/comment', commentRoutes)

// routes Recommend
const recommendRoutes = require("./routes/recommend.routes");
app.use('/api/recommend', recommendRoutes)

// catch all not found response
app.use('*', function(_, res) {
    res.status(404).end()
})

// error responses
app.use('*', function(err, req, res, next) {
    console.log('hi')
    console.error(`${err.name}: ${err.message}`)
    // console.error(err)
    next(err)
})

app.use('*', errors.handlers)

app.use('*', function(err, req, res, next) {
    res.status(500).json({
        message: 'something really unexpected happened'
    })
})

// export the app object for use elsewhere
module.exports = app