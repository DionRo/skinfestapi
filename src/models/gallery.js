const { func } = require('joi');
const mongoose = require('mongoose');
const BlogPost = require('./blogpost');

const Schema = mongoose.Schema;

const GallerySchema = new Schema({
    name: {
        type: String,
        required: [true, 'A gallery needs some a name.']
    },
    about: {
        type: String,
        required: [true, 'A gallery needs some information.']
    },
    posts: {
        type: [BlogPost],
        default: []
    }
}, {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }   
});

GallerySchema.virtual('totalViewCount').get(function() {
    let count = 0;
    
    for (i = 0; i < this.posts.length; i++) {
        count +=  this.posts[i].viewAmount;
    }
    return count;
});

module.exports = mongoose.model('Gallery', GallerySchema);