const mongoose = require('mongoose');
const Comment = require('./comment');

const Schema = mongoose.Schema;

const BlogPostSchema = new Schema({
    title: String,
    content: String,
    image: String,
    hasAdultAge: Boolean,
    likes: Number,
    viewAmount: Number,
    isVisable: Boolean,
    createdAt: String,
    comments: {
        type: [Comment],
        default: []
    }
});


module.exports = BlogPostSchema;
