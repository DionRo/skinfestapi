const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    author: String,
    content: String,
    createdAt: String
});

module.exports = CommentSchema;
