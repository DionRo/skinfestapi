const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A user needs a name.']
    },
    email: {
        type: String,
        required: [true, 'A user needs an email.']
    },
    password: {
        type: String,
        required: [true, 'A user needs an password.']
    },
    dob: {
        type: Date,
        required: [true, 'A user needs an date of birth.']
    },
    gallery:   {
        type: Schema.Types.ObjectId,
        ref: "Gallery"
    },
    roles: [
        {
          type: Schema.Types.ObjectId,
          ref: "Role",
        }
      ]
});

module.exports = mongoose.model('User', UserSchema);