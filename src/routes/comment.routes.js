// Init
const express = require('express');
const router = express.Router();

// Middleware
const verifyJwt = require("../middlewares/verifyJwt");
const verifyComment = require("../middlewares/verifyComment");

// Controller setup
const CommentController = require("../controllers/comment.controller");

router.post("/:id/create",  [verifyJwt.verifyToken, verifyComment.validateBody], CommentController.create);
router.delete("/:id/:postId/delete",  [verifyJwt.verifyToken], CommentController.destroy);


module.exports = router; 

