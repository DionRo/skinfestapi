// Init
const express = require('express');
const router = express.Router();

// Middleware
const verifyJwt = require("../middlewares/verifyJwt");

// Controller setup
const GalleryController = require("../controllers/gallery.controller");

router.get("/all", GalleryController.allGalleries)
router.get("/top", GalleryController.galleryTop)
router.get("/myGallery", verifyJwt.verifyToken, GalleryController.myGallery)
router.get("/:id", GalleryController.galleryById)


module.exports = router;

