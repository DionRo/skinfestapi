// Init
const express = require('express');
const router = express.Router();

// Middleware
const verifyJwt = require("../middlewares/verifyJwt");
const verifyAccount = require("../middlewares/verifyAccount");

// Controller setup
const UserController = require("../controllers/user.controller");

router.put("/edit", [verifyJwt.verifyToken, verifyAccount.checkDuplicateEmail, verifyAccount.validateBody], UserController.update)
router.delete("/delete", UserController.destroy);

module.exports = router;

