// Init
const express = require('express');
const router = express.Router();

// Middleware
const verifyJwt = require("../middlewares/verifyJwt");
const verifyPost = require("../middlewares/verifyPost");


// Controller setup
const BlogPostController = require("../controllers/blog.controller");

router.post("/createPost",  [verifyJwt.verifyToken,verifyPost.validateBody], BlogPostController.create);
router.get("/post/:id", BlogPostController.get);
router.put("/post/:id/update", [verifyJwt.verifyToken,verifyPost.validateBody], BlogPostController.update);
router.put("/post/:id/vote", [verifyJwt.verifyToken], BlogPostController.vote);
router.delete("/post/:id/delete", [verifyJwt.verifyToken], BlogPostController.destroy);


module.exports = router; 

