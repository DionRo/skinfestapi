// Init
const express = require('express');
const router = express.Router();

// Middleware
const verifyJwt = require("../middlewares/verifyJwt");

// Controller setup
const RecommendController = require("../controllers/recommend.controller");

router.get("/get", verifyJwt.verifyToken,RecommendController.getRecommendations)
router.post("/create", verifyJwt.verifyToken, RecommendController.create)
router.delete("/delete", verifyJwt.verifyToken, RecommendController.destroy)

module.exports = router; 

