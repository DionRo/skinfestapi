// Init
const express = require('express');
const router = express.Router();

// Middleware
const verifySignUp = require("../middlewares/verifySignUp");
const verifyLogin = require("../middlewares/verifyLogin");


// Controller setup
const AuthController = require("../controllers/auth.controller");

router.post("/signup",  [verifySignUp.validateBody, verifySignUp.checkDuplicateEmail], AuthController.signUp)
router.post("/signin", verifyLogin.validateBody , AuthController.login)

module.exports = router;

