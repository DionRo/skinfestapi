const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const moment = require("moment");


const User = require("../models/user");
const Gallery = require("../models/gallery");
const Comment = require("../models/comment");

async function create(req,res){
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try{
        const id = req.params.id
        const user = await User.findById(information._id);
        const gallery = await Gallery.findOne({"posts._id": id});

        let date = moment(new Date()).format('LLLL');
        req.body.createdAt = date;
        req.body.author = user.name;

        const blogPost = gallery.posts.id(id);
        const comment = req.body
        blogPost.comments.push(comment);
        
        gallery.markModified('posts');
        await gallery.save()

        const resp = await blogPost.comments.splice(-1);

        res.status(200).json({data: resp});

    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

async function destroy(req,res)
{
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try{
        const id = req.params.id
        const postId = req.params.postId

        const user = await User.findOne({_id: information._id}).populate({path: 'roles'})
        const gallery = await Gallery.findOne({"posts._id": postId});

        if(user.roles[1].name !== 'admin'){
            return res.status(401).json({ error: "Access denied" });
        } 

        const blogPost = gallery.posts.id(postId);
        const comments = blogPost.comments.id(id).remove();

        gallery.markModified('posts');
        await gallery.save()

        res.status(200).json({data: [blogPost, gallery, blogPost.comments]});

    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

module.exports = {
    create,
    destroy 
};