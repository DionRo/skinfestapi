const jwt = require("jsonwebtoken");
const User = require("../models/user");
const Gallery = require("../models/gallery");

async function allGalleries(req,res) {
    const galleries = 
    await User.find()
      .populate({
        path: 'gallery'
      })
    res.status(200).json({ error: null, data: galleries });
}

async function myGallery(req,res) {

    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    const gallery = 
    await User.findById(information._id)
      .populate({
        path: 'gallery'
      })
    res.status(200).json({ error: null, data: gallery });
    
}

async function galleryById(req,res) {
  try{
    const id = req.params.id
    const gallery = await Gallery.findOne({_id: id});
    res.status(200).json({data: gallery});
  }
  catch(error){
      console.log(error);
      res.status(400).json({ error });
  }
}

async function galleryTop(req,res){
  try{
    const galleries = await Gallery.find({}).limit(3)
    res.status(200).json({data: galleries});
  }
  catch(error){
      console.log(error);
      res.status(400).json({ error });
  }
}

module.exports = {
    allGalleries,
    myGallery,
    galleryById,
    galleryTop,
}