const jwt = require("jsonwebtoken");
const moment = require("moment");

const User = require("../models/user");
const Gallery = require("../models/gallery");

async function create(req,res) {
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try{
        delete req.body._id;
        let date = moment(new Date()).format('LL');
        req.body.createdAt = date;
        const user = await User.findById(information._id);
        const gallery = await Gallery.findById(user.gallery._id);
        
       gallery.posts.push(req.body);
       const savedGallery = await gallery.save();

        res.status(200).json({data: 'Success'})
    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

async function get(req,res) {

    try{
        const id = req.params.id
        const gallery = await Gallery.findOne({"posts._id": id});
        
        const blogPost = gallery.posts.id(id);
        let viewAmount = blogPost.viewAmount + 1;
        blogPost.viewAmount = viewAmount;
        gallery.markModified('posts');
        await gallery.save()

        res.status(200).json({data: [blogPost, gallery, blogPost.comments]});
    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

async function update(req,res){
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try{
        const id = req.params.id
        const user = await User.findById(information._id);
        const gallery = await Gallery.findOne({_id: user.gallery._id});

        const blogPost = gallery.posts.id(id);
        blogPost.title = req.body.title;
        blogPost.content = req.body.content;
        blogPost.image = req.body.image;
        blogPost.hasAdultAge = req.body.hasAdultAge;

        gallery.markModified('posts');
        await gallery.save()
        
        res.status(200).json({data: 'Success'})
    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

async function destroy(req,res){
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try{
        const id = req.params.id
        const user = await User.findById(information._id);
        const gallery = await Gallery.findOne({_id: user.gallery._id});

        const blogPost = gallery.posts.id(id).remove();
        gallery.markModified('posts');
        await gallery.save()
        
        res.status(200).json({data: 'Success'})
    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

async function vote(req,res){

    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try{
        const id = req.params.id
        const gallery = await Gallery.findOne({"posts._id": id});
        const blogPost = gallery.posts.id(id);

        if(req.body.type === "UP"){
            let likes = blogPost.likes + 1;
            blogPost.likes = likes;    
        }else{
            let likes = blogPost.likes - 1;
            blogPost.likes = likes;
        }
        gallery.markModified('posts');
        await gallery.save()

        
        res.status(200).json({data: blogPost})
    }catch(error){
        console.log(error);
        res.status(400).json({ error });
    }
}

module.exports = {
    create, 
    get,
    update,
    destroy,
    vote,
};