const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user");
const Role = require("../models/role");
const Gallery = require("../models/gallery");

const neo = require('../../neo');
const gallery = require("../models/gallery");
const user = require("../models/user");


async function getRecommendations(req,res) {
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try {
        const user = await User.findById(information._id);

        const session = neo.session()

        const myLikes = await session.run("match (u:User {id: $id})-[:Liked]->(galleries) return galleries", {id: user._id.toString()});
        const recommends = await session.run("match (u:User {id: $id})-[:Liked]->(gallery)-[:Liked]-(user)-[:Liked]->(galleries) return galleries", {id: user._id.toString()});

        session.close();

        const gIds = []

        for (let index = 0; index < recommends.records.length; index++) {
            let needsAdd = true
            const rId =  recommends.records[index]._fields[0].properties.id;
            for (let index = 0; index < myLikes.records.length; index++) {
                const mId = myLikes.records[index]._fields[0].properties.id;
                if (rId === mId){
                    needsAdd = false; 
                } 
            }

            if(needsAdd){
                gIds.push(rId);
            }
        }

        
        const galleries = []

        for (let index = 0; index < gIds.length; index++) {
            const gallery = await Gallery.findById(gIds[index]);
            if(gallery.posts.length != 0){
                galleries.push(gallery);            
            }
        }      
        
        res.status(200).json({data: galleries});
    
      } catch (error) { 
        console.log(error)
        res.status(400).json({ error });
      }
}

async function create(req,res){
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try {
        const userId = information._id;
        const galleryId = req.body.galleryId;

        const session = neo.session()
        const exist =  await session.run("match (u:User {id: $id}) match (g:Gallery {id: $gId}) RETURN EXISTS((u)-[:Liked]-(g)) ", {id: userId, gId:galleryId})
        console.log(exist.records[0]._fields[0]);
        if(!exist.records[0]._fields[0]){
            await session.run("match (u:User {id: $id}) match (g:Gallery {id: $gId}) create (u)-[l:Liked]->(g)", {id: userId, gId:galleryId})
            session.close();
            res.status(200).json({ error: null, data: galleryId });
        }else{
            session.close();
            res.status(200).json({ error: null, data: 'Already Added' });

        }
      } catch (error) { 
        console.log(error) 
        res.status(400).json({ error });
      }
}

async function destroy(req,res){
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try {
        const userId = information._id;
        const galleryId = req.body.galleryId;

        const session = neo.session()
        const exist =  await session.run("MATCH (n { id: $id })-[l:Liked]->(gallery {id: $gId}) delete l", {id: userId, gId:galleryId})
        session.close();

        res.status(200).json({ error: null, data: 'Delete Done' });

      }catch (error) { 
        console.log(error) 
        res.status(400).json({ error });
      }
}

module.exports = {
    getRecommendations, 
    create,
    destroy
};