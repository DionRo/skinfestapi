const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user");
const Role = require("../models/role");
const Gallery = require("../models/gallery");

const neo = require('../../neo')


async function signUp(req, res){
  const salt = await bcrypt.genSalt(10);
  const password = await bcrypt.hash(req.body.password, salt);

  const gallery = new Gallery({
    name: req.body.galleryName,
    about: req.body.galleryAbout
  });

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    dob: req.body.dob,
    password: password,
    gallery: gallery,
  });

  try {
    const savedUser = await user.save();
    const roles = await Role.find({name: { $in: req.body.roles}});
    savedUser.roles = roles.map(role => role._id);
    await gallery.save();

    await User.findByIdAndUpdate(savedUser._id, savedUser);

    const session = neo.session()

    await session.run("create (n:User {id: $id ,name: $name})", {id: savedUser._id.toString(),name: req.body.name,});
    await session.run("create (n:Gallery {id: $id, name: $galleryName})", {id: savedUser.gallery._id.toString() ,galleryName: savedUser.gallery.name})
    await session.run("match (u:User {id: $id}) match (g:Gallery {id: $gId}) create (u)-[l:Liked]->(g)", {id: savedUser._id.toString(), gId:savedUser.gallery._id.toString()})
    session.close();

   res.status(200).json({ error: null, data: savedUser });

  } catch (error) { 
    console.log(error)
    res.status(400).json({ error });
  }
}

async function login(req,res){
  const user = 
    await User.findOne({ email: req.body.email })
      .populate({
        path: 'roles'
      })
      .populate({
        path: 'gallery'
      })
  
  // throw error when email is wrong
  if (!user) return res.status(400).json({ error: "Incorrect Credentials, please check email" });

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
  return res.status(400).json({ error: "Incorrect Credentials, please check password" });


  var authorities = [];

  for (let i = 0; i < user.roles.length; i++) {
    authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
  }
    
  const token = jwt.sign(
    // payload data
    {
      _id: user._id,
    },
    process.env.TOKEN_SECRET
  );

  res.status(200).json({
    _id: user._id,
    name: user.name,
    email: user.email,
    dob: user.dob,
    roles: authorities,
    gallery: user.gallery,
    accessToken: token
  });

}

module.exports = {
   signUp,
   login
};