const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const neo = require('../../neo')


const User = require("../models/user");
const Gallery = require("../models/gallery");

async function update(req,res) {
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    delete req.body.roles;

    if(req.body.password){
        const salt = await bcrypt.genSalt(10);
        const password = await bcrypt.hash(req.body.password, salt);
        req.body.password = password;
    }

    try {
        const gallery = await Gallery.updateOne({_id: req.body.gallery._id}, {name: req.body.gallery.name, about: req.body.gallery.about})
        const user = await User.update({_id: information._id}, req.body);
        
        const userF = 
          await User.findOne({ email: req.body.email })
            .populate({
              path: 'roles'
            })
            .populate({
              path: 'gallery'
        });

        const token = jwt.sign(
          // payload data
          {
            _id: userF._id,
          },
          process.env.TOKEN_SECRET
        );

        var authorities = [];

        for (let i = 0; i < userF.roles.length; i++) {
          authorities.push("ROLE_" + userF.roles[i].name.toUpperCase());
        }

        const session = neo.session()

        await session.run("match (u:User {id: $id}) set u.name = $name", {id: userF._id.toString(), name: userF.name});
        await session.run("match (g:Gallery {id: $id}) set g.name = $name", {id: userF.gallery._id.toString(), name: userF.gallery.name});
        session.close();

        
        res.status(200).json({
          _id: userF._id,
          name: userF.name,
          email: userF.email,
          dob: userF.dob,
          roles: authorities,
          gallery: userF.gallery,
          accessToken: token
        });
    
      } catch (error) { 
        res.status(400).json({ error });
      }
}

async function destroy(req,res){
    let token =  req.headers.authorization;
    token = token.replace('Bearer ', '')
    const information = jwt.decode(token);

    try {
        const user = await User.findById(information._id);

        const session = neo.session()
        await session.run("match (u:User {id: $id}) detach delete u", {id:information._id.toString()});
        session.close();

        const gallery = await Gallery.findByIdAndDelete(user.gallery._id);
        user.remove();
        res.json({ error: null, data: user });
    
      } catch (error) { 
        console.log(error)
        res.status(400).json({ error });
      }
}

module.exports = {
    update, 
    destroy,
};